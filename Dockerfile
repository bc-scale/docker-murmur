FROM alpine:3 as builder

ARG VERSION=1.4.230

LABEL \
    org.label-schema.vendor="Alexey Skobkin - skobkin-ru@ya.ru" \
    org.label-schema.url="https://gitlab.com/skobkin/docker-murmur" \
    org.label-schema.name="Murmur Server" \
    org.label-schema.version=$VERSION \
    org.label-schema.vcs-url="https://gitlab.com/skobkin/docker-murmur.git" \
    org.label-schema.license="MIT" \
    org.label-schema.schema-version="1.0"

WORKDIR /tmp/murmur

# openssl3-dev qt5-qtsvg-dev avahi-dev git
RUN set -x \
    apk update -q --no-cache --no-progress && \
    apk add -q --no-cache --no-progress curl

RUN \
    curl -sSL https://github.com/mumble-voip/mumble/releases/download/v${VERSION}/mumble-${VERSION}.tar.gz -o /tmp/mumble.tar.gz && \
    tar -zxf /tmp/mumble.tar.gz --strip-components=1 -C /tmp/murmur

RUN \
    apk add -q --no-cache --no-progress boost-dev build-base cmake libcap-dev protobuf-dev qt5-qtbase-dev

RUN \
    cd /tmp/murmur && \
    mkdir build && \
    cd build && \
    # static build is currently unavailable
    # -Dstatic=ON
    cmake -Dclient=OFF -Ddbus=OFF -Dice=OFF -Dzeroconf=OFF .. && \
    make -j $(nproc)

FROM alpine:3

RUN \
    apk update -q --no-cache && \
    apk add -q --no-cache libcap libcrypto1.1 libgcc libprotobuf libssl1.1 libstdc++ musl qt5-qtbase qt5-qtbase-sqlite \
      shadow tzdata && \
    mkdir -p /ssl && \
    mkdir -p /config && \
    mkdir -p /data

COPY --from=builder /tmp/murmur/build/mumble-server /bin/mumble-server

EXPOSE 64738/tcp 64738/udp

VOLUME ["/config", "/data", "/ssl"]

ENTRYPOINT ["/bin/mumble-server", "-ini", "/config/murmur.ini", "-v", "-fg"]
